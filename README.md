# Car Charging Service

This is a demo project, a microservice which exposes RESTful API for 
<li> Start a charging chargingSession.
<li> Stop a charging chargingSession.
<li> Get all charging sessions.
<li> Get statistics of last minute (how many requests were made, how many to start chargin, how many to stop charging).

###Design Consideration:
Currently storing sessions in in-memory data structure, a HashMap.

<br>Since we need just the counting of requests to start and stop API, I've stored the counter in a Map per second.

<br>Since we are storing the counters per second, we need to remove older entries otherwise it will just grow on enormously.

Runtime Complexity:
    Create new chargingSession O(1); 
    Stop Session O(1); 
    Get All Sessions O(N); 
    Get Summary O(1);  
    
 
###How to Run:
Choose one of the following options.
<li> A fat jar has been created in /binary folder. Just run <code>'java -jar car-charging-service.jar'</code> on command line. Fat JAR can be created using gradle command <code>'gradle bootJar'</code>
<li> Import the project in IntelliJ or Eclipse and run Application.java file. 
<li> If you have gradle installed, run command <code>'gradle  bootRun'</code>

