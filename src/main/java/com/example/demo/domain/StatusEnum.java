package com.example.demo.domain;

public enum StatusEnum {
    IN_PROGRESS, FINISHED
}
