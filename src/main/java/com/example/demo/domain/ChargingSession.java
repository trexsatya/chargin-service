package com.example.demo.domain;

import com.example.demo.utils.CustomLocalDateTimeDeserializer;
import com.example.demo.utils.CustomLocalDateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

public class ChargingSession {
    private UUID id;
    private String stationId;

    private LocalDateTime startedAt;

    private LocalDateTime stoppedAt;

    private StatusEnum status;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public LocalDateTime getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(LocalDateTime startedAt) {
        this.startedAt = startedAt;
    }

    public LocalDateTime getStoppedAt() {
        return stoppedAt;
    }

    public void setStoppedAt(LocalDateTime stoppedAt) {
        this.stoppedAt = stoppedAt;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChargingSession chargingSession = (ChargingSession) o;
        return Objects.equals(getId(), chargingSession.getId()) &&
                Objects.equals(getStationId(), chargingSession.getStationId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getStationId());
    }

    public static class Builder {
        private UUID id;
        private String stationId;

        private LocalDateTime startedAt;

        private LocalDateTime stoppedAt;

        private StatusEnum status;

        public static Builder aChargingSession(){
            return new Builder();
        }

        public Builder withId(UUID id){
            this.id = id;
            return this;
        }

        public Builder withStationId(String id){
            this.stationId = id;
            return this;
        }

        public Builder startedAt(LocalDateTime dateTime){
            this.startedAt = dateTime;
            return this;
        }

        public Builder stoppedAt(LocalDateTime dateTime){
            this.stoppedAt = dateTime;
            return this;
        }

        public Builder withStatus(StatusEnum status){
            this.status = status;
            return this;
        }

        public ChargingSession build(){
            ChargingSession session = new ChargingSession();
            session.setId(id);
            session.setStationId(stationId);
            session.setStartedAt(startedAt);
            session.setStoppedAt(stoppedAt);
            session.setStatus(status);
            return session;
        }
    }

    @Override
    public String toString() {
        return "ChargingSession{" +
                "id=" + id +
                ", stationId='" + stationId + '\'' +
                ", startedAt=" + startedAt +
                ", stoppedAt=" + stoppedAt +
                ", status=" + status +
                '}';
    }
}
