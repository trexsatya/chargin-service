package com.example.demo.domain;

public class Statistics {
    private Long totalCount, startedCount, stoppedCount;

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    public Long getStartedCount() {
        return startedCount;
    }

    public void setStartedCount(Long startedCount) {
        this.startedCount = startedCount;
    }

    public Long getStoppedCount() {
        return stoppedCount;
    }

    public void setStoppedCount(Long stoppedCount) {
        this.stoppedCount = stoppedCount;
    }
}
