package com.example.demo.domain;

import org.springframework.http.HttpStatus;

public class ApiError {
    private HttpStatus status;
    private String errorString;
    private Exception exception;

    public ApiError(HttpStatus status, String errorString, Exception exception) {
        this.status = status;
        this.errorString = errorString;
        this.exception = exception;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getErrorString() {
        return errorString;
    }

    public void setErrorString(String errorString) {
        this.errorString = errorString;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }
}
