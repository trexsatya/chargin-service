package com.example.demo.controllers;

import com.example.demo.controllers.exceptions.BadRequestException;
import com.example.demo.controllers.exceptions.ResourceNotFoundException;
import com.example.demo.domain.ChargingSession;
import com.example.demo.domain.Statistics;
import com.example.demo.services.ChargingSessionService;
import com.example.demo.utils.Pair;
import com.example.demo.validators.SessionValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class SessionController {
    private Logger logger = LoggerFactory.getLogger(SessionController.class);

    @Autowired
    private ChargingSessionService chargingSessionService;

    @RequestMapping(value = "/chargingSessions",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ChargingSession newSession(@RequestBody ChargingSession chargingSession) {
        logger.debug("newSession: stationId={}", chargingSession.getStationId());
        Pair<String, Boolean> validateSessionId = SessionValidator.validateSessionId(chargingSession);
        if(!validateSessionId.getRight()) {
            throw new BadRequestException(validateSessionId.getLeft());
        }

        return chargingSessionService.createSession(chargingSession.getStationId());
    }

    @RequestMapping(value = "/chargingSessions",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<ChargingSession> getSessions(){
        logger.debug("getSessions()");
        Stream<ChargingSession> returnSession = chargingSessionService.getAllSessions();
        return returnSession.collect(Collectors.toList());
    }

    @RequestMapping(value = "/chargingSessions/summary",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Statistics getSummary(){
        logger.debug("getSummary()");
        return chargingSessionService.getStatistics();
    }

    @RequestMapping(value = "/chargingSessions/{id}",method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ChargingSession stopSession(@PathVariable("id") String id){
        logger.debug("stopSession(): id={}", id);
        if(chargingSessionService.getSession(id) == null) throw new ResourceNotFoundException("ChargingSession not found by id "+id);

        chargingSessionService.stopSession(id);

        return chargingSessionService.getSession(id);
    }


}
