package com.example.demo.services;

import com.example.demo.domain.ChargingSession;
import com.example.demo.domain.Statistics;
import com.example.demo.domain.StatusEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

import static com.example.demo.services.StatisticsService.EventType.CHARGING_SESSION_FINISHED;
import static com.example.demo.services.StatisticsService.EventType.CHARGING_SESSION_STARTED;

@Service
public class InMemoryChargingSessionServiceImpl implements ChargingSessionService {
    private static final Logger logger = LoggerFactory.getLogger(InMemoryChargingSessionServiceImpl.class);

    private Map<String, ChargingSession> sessionIds = new HashMap<>();

    private StatisticsService<ChargingSession> statisticsService;

    public InMemoryChargingSessionServiceImpl(@Autowired StatisticsService<ChargingSession> statisticsService) {
        this.statisticsService = statisticsService;
    }

    @Override
    public ChargingSession createSession(String stationId) {
        LocalDateTime now = LocalDateTime.now();
        ChargingSession chargingSession = ChargingSession.Builder.aChargingSession()
                .withId(UUID.randomUUID())
                .withStationId(stationId)
                .startedAt(now)
                .withStatus(StatusEnum.IN_PROGRESS)
                .build();

        sessionIds.put(chargingSession.getId().toString(), chargingSession);

        logger.debug("Created charging session: {}", chargingSession);

        statisticsService.handleEvent(CHARGING_SESSION_STARTED, chargingSession);
        return chargingSession;
    }

    @Override
    public boolean stopSession(String sessionId) {
        ChargingSession chargingSession = sessionIds.get(sessionId);
        if( chargingSession == null) {
            logger.debug("Did not find charging session with id {}", sessionId);
            return false;
        }

        chargingSession.setStatus(StatusEnum.FINISHED);
        statisticsService.handleEvent(CHARGING_SESSION_FINISHED, chargingSession);

        return true;
    }

    @Override
    public ChargingSession getSession(String sessionId) {
        return sessionIds.get(sessionId);
    }

    @Override
    public Stream<ChargingSession> getAllSessions() {
        return sessionIds.entrySet().stream().map(Map.Entry::getValue);
    }

    @Override
    public Statistics getStatistics() {
       return statisticsService.getStatistics(1, TimeUnit.MINUTES);
    }
}
