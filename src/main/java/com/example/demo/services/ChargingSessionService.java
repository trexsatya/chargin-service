package com.example.demo.services;

import com.example.demo.domain.ChargingSession;
import com.example.demo.domain.Statistics;

import java.util.stream.Stream;

public interface ChargingSessionService {
    /**
     * Create a new session.
     * @param stationId stationId
     * @return created session.
     */
    ChargingSession createSession(String stationId);

    /**
     * stop session if exists, else return false.
     * @param session session.
     * @return true if session was successfully stopped. false if couldn't stop.
     */
    boolean stopSession(String session);

    ChargingSession getSession(String sessionId);

    Stream<ChargingSession> getAllSessions();

    /**
     * return statistics of last minute.
     * @return statistics.
     */
    Statistics getStatistics();
}
