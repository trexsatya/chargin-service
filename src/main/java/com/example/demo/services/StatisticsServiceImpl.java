package com.example.demo.services;

import com.example.demo.domain.ChargingSession;
import com.example.demo.domain.Statistics;
import com.example.demo.utils.SimpleCacheWithExpiration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Predicate;

/**
 * This implementation keeps a running statistics of events. It also discards events which are older than required.
 * It maintains count of events per second.
 */
@Component
public class StatisticsServiceImpl implements StatisticsService<ChargingSession> {
    private static final Logger logger = LoggerFactory.getLogger(StatisticsServiceImpl.class);

    private static final int MILLISECONDS_PER_SECOND = 1000;

    /*
     * capacity = number of seconds for which statistics must be maintained. Default capacity is 60 seconds.
     */
    private final int capacity;

    public StatisticsServiceImpl() {
        this.capacity = 60; //in seconds
    }

    public StatisticsServiceImpl(int capacity) {
        this.capacity = capacity;
    }

    private Predicate<Map.Entry<Long, AtomicLong>> expirationTest(){
        return entry -> {
            long secondsTillNow = toSeconds(Instant.now());
            long entryTimeInSeconds = entry.getKey();
            boolean isOld = secondsTillNow - entryTimeInSeconds > capacity;

            if(isOld) logger.debug("{} is old removing it. {}, {}",secondsTillNow, entryTimeInSeconds);

            return isOld;
        };
    }

    /**
     * Counters
     */
    private final Map<Long, AtomicLong> sessionsStarted = new SimpleCacheWithExpiration<>(expirationTest());
    private final Map<Long, AtomicLong> sessionsStopped = new SimpleCacheWithExpiration<>(expirationTest());


    @Override
    public Statistics getStatistics(int numUnits, TimeUnit unit) {
        Statistics statistics = new Statistics();
        long startedCount = 0;
        long stoppedCount = 0;

        long secondsAsOfNow = toSeconds(Instant.now());
        for (int i = 0; i < unit.toSeconds(numUnits); i++) {
            AtomicLong atomicLong = sessionsStarted.getOrDefault(secondsAsOfNow - i, new AtomicLong(0));

            startedCount += atomicLong.longValue();

            AtomicLong stopped = sessionsStopped.getOrDefault(secondsAsOfNow - i, new AtomicLong(0));
            stoppedCount += stopped.longValue();
        }

        statistics.setStartedCount(startedCount);
        statistics.setStoppedCount(stoppedCount);
        statistics.setTotalCount(startedCount+stoppedCount);

        return statistics;
    }

    @Override
    public void handleEvent(EventType eventType, ChargingSession session) {
        if(eventType == EventType.CHARGING_SESSION_STARTED) {
            handleChargingSessionStart(session);
        } else if(eventType == EventType.CHARGING_SESSION_FINISHED) {
            handleChargingSessionFinish(session);
        }
    }

    private void handleChargingSessionFinish(ChargingSession session) {
        LocalDateTime now = LocalDateTime.now();
        long entryTimeInSeconds = toSeconds(now.atZone(ZoneId.systemDefault()).toInstant());

        synchronized (sessionsStopped) {
            if(sessionsStopped.get(entryTimeInSeconds) == null){
                sessionsStopped.put(entryTimeInSeconds, new AtomicLong(0));
            }
        }

        sessionsStopped.get(entryTimeInSeconds).getAndIncrement();
    }

    private void handleChargingSessionStart(ChargingSession session) {
        LocalDateTime now = LocalDateTime.now();
        long entryTimeInSeconds = toSeconds(now.atZone(ZoneId.systemDefault()).toInstant());

        synchronized (sessionsStarted) {
            if(sessionsStarted.get(entryTimeInSeconds) == null) {
                sessionsStarted.put(entryTimeInSeconds, new AtomicLong(0));
            }
        }

        sessionsStarted.get(entryTimeInSeconds).getAndIncrement();
    }

    private long toSeconds(Instant instant){
        return instant.toEpochMilli() / MILLISECONDS_PER_SECOND;
    }
}
