package com.example.demo.services;

import com.example.demo.domain.Statistics;

import java.util.concurrent.TimeUnit;

/**
 * A service which will handle different domain events and perform computations necessary for getting statistics.
 */
public interface StatisticsService<T> {
    enum EventType {
        CHARGING_SESSION_STARTED, CHARGING_SESSION_FINISHED
    }

    Statistics getStatistics(int numUnits, TimeUnit seconds);
    void handleEvent(EventType eventType, T data);
}
