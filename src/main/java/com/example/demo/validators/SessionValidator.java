package com.example.demo.validators;

import com.example.demo.domain.ChargingSession;
import com.example.demo.utils.Pair;

public class SessionValidator {

    /**
     * First argument of the pair returned contains error message; second one contains whether field is valid.
     * Pair<X,Y> construct can be replaced by Either monads provided by libraries such as Vavr if requirement grows.
     * @param chargingSession
     * @return
     */
    public static Pair<String, Boolean> validateSessionId(ChargingSession chargingSession){
        if(chargingSession.getStationId() == null || chargingSession.getStationId().trim().isEmpty()) {
            return new Pair<>("Station id can't be empty!", false);
        }

        return new Pair<>("", true);
    }
}
