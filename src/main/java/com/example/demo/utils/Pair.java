package com.example.demo.utils;

import java.util.Objects;

public class Pair<X,Y> {
    private X left;
    private Y right;

    public Pair(X left, Y right) {
        this.left = left;
        this.right = right;
    }

    public X getLeft() {
        return left;
    }

    public void setLeft(X left) {
        this.left = left;
    }

    public Y getRight() {
        return right;
    }

    public void setRight(Y right) {
        this.right = right;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return Objects.equals(getLeft(), pair.getLeft()) &&
                Objects.equals(getRight(), pair.getRight());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getLeft(), getRight());
    }
}
