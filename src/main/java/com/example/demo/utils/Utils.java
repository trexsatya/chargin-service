package com.example.demo.utils;

public class Utils {

    public static boolean isNullOrEmpty(String string){
        return string == null || string.isEmpty();
    }
}
