package com.example.demo.utils;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Predicate;

public class SimpleCacheWithExpiration<K,V> extends LinkedHashMap<K,V>{
    private Predicate<Map.Entry<K,V>> predicate;

    public SimpleCacheWithExpiration(Predicate<Map.Entry<K, V>> predicate) {
        this.predicate = predicate;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<K,V> eldest) {
        return predicate.test(eldest);
    }
}
