package com.example.demo;

import com.example.demo.domain.ChargingSession;
import com.example.demo.domain.Statistics;
import com.example.demo.services.InMemoryChargingSessionServiceImpl;
import com.example.demo.services.ChargingSessionService;
import com.example.demo.services.StatisticsServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class ChargingChargingSessionServiceTest {
    Logger logger = LoggerFactory.getLogger(ChargingChargingSessionServiceTest.class);

    private ChargingSessionService chargingSessionService;

    @Before
    public void setUp() throws Exception {
        chargingSessionService = new InMemoryChargingSessionServiceImpl(new StatisticsServiceImpl());
    }

    @Test
    public void testStatistics_ThreadSafety() throws InterruptedException {
        int NUM = 1000;
        CountDownLatch latch = new CountDownLatch(NUM);
        AtomicInteger startedCount = new AtomicInteger(0);
        AtomicInteger stoppedCount = new AtomicInteger(0);
        BlockingQueue<String> sessionsStarted = new LinkedBlockingQueue<>();

        IntStream.range(0,NUM).forEach(i -> {
              new Thread(()-> {
                  if(random.nextInt() % 2 == 0) {
                      String stationId = "S-" + i;
                      ChargingSession session = chargingSessionService.createSession(stationId);
                      startedCount.getAndIncrement();
                      sessionsStarted.offer(session.getId().toString());
                      logger.debug("started = {}", stationId);
                  } else if(i % 3 == 0) {
                      //Stop a session randomly
                      String sessionId = null;
                      try {
                          sessionId = sessionsStarted.poll(1000, TimeUnit.MILLISECONDS);
                      } catch (InterruptedException e) {
                          e.printStackTrace();
                      }
                      logger.debug("stopping = {}", sessionId);
                      boolean stopped = chargingSessionService.stopSession(sessionId);
                      if(stopped) stoppedCount.getAndIncrement();
                  }

                  latch.countDown();
              }).start();
            });

        latch.await();

        Statistics statistics = chargingSessionService.getStatistics();

        logger.info("started count = {}, stopped count = {}", startedCount, stoppedCount);

        Assert.assertEquals(startedCount.longValue(), (long)statistics.getStartedCount());
        Assert.assertEquals(stoppedCount.longValue(), (long)statistics.getStoppedCount());
    }

    private Random random = new Random();
    private int randomNumber(int min, int max){
        return random.nextInt(max - min + 1) + min;
    }
}
