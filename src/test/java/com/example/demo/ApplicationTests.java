package com.example.demo;

import com.example.demo.services.ChargingSessionService;
import com.jayway.jsonpath.JsonPath;
import org.hamcrest.MatcherAssert;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration(classes = {Application.class})
public class ApplicationTests {
	@Autowired
	private MockMvc mockMvc;

	@Test
	public void shouldNotCreateNewSession_InvalidSessionId() throws Exception {
		mockMvc.perform(post("/chargingSessions")
				.content(sessionRequestForSessionId(""))
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isBadRequest());
	}

	@Test
	public void shouldThrowResourceNotFound_InvalidStopCall() throws Exception {
	    mockMvc.perform(put("/chargingSessions/not-existing" ))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();
    }

    @Test
    public void endToEndTest() throws Exception {
        String id1 = callApiToCreateSession("ABCD-1234");
        String id2 = callApiToCreateSession("ABCD-12345");

        List<String> list = callApiToFetchAllSessions();
        MatcherAssert.assertThat(list, containsInAnyOrder(id1, id2));

        String response = callApiToStopSession(id1);
        Assert.assertEquals("FINISHED", JsonPath.parse(response).read("$.status"));

        String id3 = callApiToCreateSession("ABCD-123456");

        String summary = mockMvc.perform(get("/chargingSessions/summary"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        Assert.assertEquals(3, (int)JsonPath.parse(summary).read("$.startedCount"));
        Assert.assertEquals(1, (int)JsonPath.parse(summary).read("$.stoppedCount"));
    }

    private String callApiToStopSession(String id1) throws Exception {
        return mockMvc.perform(put("/chargingSessions/" + id1))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andReturn().getResponse().getContentAsString();
    }

    private List<String> callApiToFetchAllSessions() throws Exception {
        String response = mockMvc.perform(get("/chargingSessions"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        return JsonPath.parse(response).read("$..id");
    }

    private String callApiToCreateSession(String stationId) throws Exception {
        String createdSession = mockMvc.perform(post("/chargingSessions")
                .content(sessionRequestForSessionId(stationId))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn().getResponse().getContentAsString();

        return JsonPath.parse(createdSession).read("$.id");
    }

    private String sessionRequestForSessionId(String id) {
        return String.format("{ \"stationId\": \"%s\" }", id);
    }
}
